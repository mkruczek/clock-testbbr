package sample;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class Controller {


    @FXML
    Label hour;

    @FXML
    Label minute;

    @FXML
    Label second;

    @FXML
    Button go;


    public void initialize() {

        hour.setText("22");
        minute.setText("58");
        second.setText("30");

        go.setOnAction(event -> {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    for (int sec = 0; sec < 60; sec++) {
                        final int secFinal = sec;
                        if(sec < 10){
                            Platform.runLater( () -> second.setText(0 + Integer.toString(secFinal)));
                        } else {
                            Platform.runLater( () -> second.setText(Integer.toString(secFinal)));
                        }
                        if(sec == 59){
                            sec= -1;
                            setTime(minute, hour);
                        }

                        try {
                            Thread.sleep(2);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();
        });

    }



    public static void setTime(Label minute, Label hour) {

        int actuallyMinute = Integer.valueOf(minute.getText()) + 1;

        if(actuallyMinute == 60){

            Platform.runLater( () -> minute.setText("00"));

            setHour(hour);

        } else if(actuallyMinute < 10){
            Platform.runLater( () -> minute.setText(0+Integer.toString(actuallyMinute)));
        } else {
            Platform.runLater( () -> minute.setText(Integer.toString(actuallyMinute)));
        }
    }

    private static void setHour(Label hour) {
        int actuallyHour = Integer.valueOf(hour.getText()) +1;
        if(actuallyHour == 24){
            Platform.runLater( () -> hour.setText("00"));
        } else if (actuallyHour < 10){
            Platform.runLater( () -> hour.setText(0+Integer.toString(actuallyHour)));
        } else {
            Platform.runLater( () -> hour.setText(Integer.toString(actuallyHour)));
        }
    }


}
